import 'dart:async';

import 'package:flutter/cupertino.dart';

class IntervalTimer {
  final List<TimerListener> listeners = [];
  final Duration _presentationTime;
  final Duration _breakTime;
  final Duration _nearlyFinishedTime;

  int counter;
  int _iteration;
  Timer _timer;

  IntervalTimer(
      {Duration presentationTime,
      Duration breakTime,
      Duration nearlyFinishedTime})
      : this._presentationTime = presentationTime,
        this._breakTime = breakTime,
        this._nearlyFinishedTime = nearlyFinishedTime,
        this.counter = presentationTime.inSeconds,
        assert(presentationTime != null &&
            breakTime != null &&
            nearlyFinishedTime != null) {
    _iteration = 0;
    startTimer();
  }

  void startTimer() {
    this._timer = Timer.periodic(Duration(milliseconds: 1000), (Timer t) {
      counter -= 1;
      this._notifyListeners();
    });
  }

  void pauseResume() {
    if (this._timer.isActive) {
      this._timer.cancel();
      this._notifyListeners();
    } else {
      startTimer();
    }
  }

  void switchMode() {
    this._timer.cancel();
    this._iteration += 1;
    if (_iteration % 2 == 0) {
      counter = this._presentationTime.inSeconds;
    } else {
      counter = this._breakTime.inSeconds;
    }
    this.startTimer();
  }

  get mode {
    if (!this._timer.isActive) {
      return TimerMode.paused;
    }
    if (counter < 0) {
      return TimerMode.toMuch;
    } else if (counter >= 0 && counter < _nearlyFinishedTime.inSeconds) {
      return TimerMode.nearlyFinished;
    } else {
      return TimerMode.running;
    }
  }

  void subscribe(TimerListener subscriber) {
    this.listeners.add(subscriber);
  }

  void unsubscribe(TimerListener subscriber) {
    this.listeners.remove(subscriber);
  }

  void _notifyListeners() {
    this.listeners.forEach((element) {
      element.update();
    });
  }
}

abstract class TimerListener {
  update();
}

enum TimerMode {
  nearlyFinished,
  running,
  toMuch,
  paused,
}
