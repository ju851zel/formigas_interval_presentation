import 'dart:async';

import 'package:flutter/material.dart';

import 'IntervalTimer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String appName = "Intervall Presentation";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CounterPage(
          presentationTime: Duration(seconds: 8 * 60),
          breakTime: Duration(seconds: 30),
          nearlyFinishedTime: Duration(seconds: 60)),
    );
  }
}

class CounterPage extends StatefulWidget {
  final Duration presentationTime;
  final Duration breakTime;
  final Duration nearlyFinishedTime;

  CounterPage(
      {@required this.presentationTime,
      @required this.breakTime,
      @required this.nearlyFinishedTime});

  @override
  _CounterPageState createState() => _CounterPageState(
      presentationTime: presentationTime,
      breakTime: breakTime,
      nearlyFinishedTime: nearlyFinishedTime);
}

class _CounterPageState extends State<CounterPage> implements TimerListener {
  final IntervalTimer timer;

  _CounterPageState({
    @required presentationTime,
    @required breakTime,
    @required nearlyFinishedTime,
  }) : timer = IntervalTimer(
          presentationTime: presentationTime,
          breakTime: breakTime,
          nearlyFinishedTime: nearlyFinishedTime,
        ) {
    this.timer.subscribe(this);
  }

  // formats the counter in mm:ss
  String formatTimerNicely() {
    if (timer.counter >= 0) {
      return '${(timer.counter ~/ 60).toString().padLeft(2, "0")}:${(timer.counter % 60).toString().padLeft(2, "0")}';
    } else {
      return '- ${(timer.counter ~/ -60).toString().padLeft(2, "0")}:${(60 - timer.counter % 60).toString().padLeft(2, "0")}';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(MyApp.appName),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Remaining time:',
              style: TextStyle(color: Colors.white),
            ),
            Text(formatTimerNicely(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 80,
                )),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: timer.pauseResume,
                    child: Text(
                        timer.mode == TimerMode.paused ? "Resume" : "Pause"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: null,
                    child: Text("Reset"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: timer.switchMode,
                    child: Text("Finish"),
                  ),
                ),
              ],
            ),
          ],
        ),
        color: color,
      ),
    );
  }

  get color {
    if (timer.mode == TimerMode.running) {
      return Colors.green;
    } else if (timer.mode == TimerMode.nearlyFinished) {
      return Colors.yellow;
    } else if (timer.mode == TimerMode.toMuch) {
      return Colors.red;
    } else {
      return Colors.blueGrey;
    }
  }

  @override
  update() {
    setState(() {});
  }
}
